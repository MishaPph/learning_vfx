﻿using UnityEngine;

[ExecuteInEditMode]
public class SetMaterial : MonoBehaviour
{
   [SerializeField] private Material[] _materials;

   [SerializeField] private string _propertyName;
   [SerializeField, Range(-6, 5)] private float _value;

   private void Update()
   {
      foreach (var mtl in _materials)
      {
         mtl.SetFloat(_propertyName, _value);
      }
   }
}
