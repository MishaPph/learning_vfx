﻿Shader "Custom/Dissolve"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        
        _DissolvePercentage("DissolvePercentage", Range(0,1)) = 0.0
        _ShowTexture("ShowTexture", Range(0,1)) = 0.0
        _Scale("Scale", Range(0.001,1.0)) = 0.0
        
        _Radius("_Radius", Range(0.001,1.0)) = 0.0
        
         _Magnitude("Magnitude", Range(0.01,10.0)) = 0.0
          _FadeLength("Fade Length", Range(0, 10)) = 0.15
    }
    SubShader
    {
        Tags {  "RenderType" = "Transparent"
            "Queue" = "Transparent"
             }
        LOD 200

        Blend SrcAlpha OneMinusSrcAlpha
        ZWrite On
        
        CGPROGRAM
        #include "SimplexNoise3D.hlsl"
        #include "UnityCG.cginc"
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        float4 _MainTex_ST;
         
        sampler2D_float _CameraDepthTexture;
        
        struct Input
        {
            float2 uv_MainTex;
            float3 worldPos;
            float3 vect;
            float4 screenPos;
            float eyeDepth;
            float2 uv;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
        half _DissolvePercentage;
        half _ShowTexture;
        half _Scale;
        half _Radius;
        half _Magnitude;
        
        float _FadeLength;
        
        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        half getgradient(half3 p) {
            return  saturate(snoise(p) * 0.5 + 0.5);
          
        }
        
        void vert (inout appdata_full v, out Input o) {
            UNITY_INITIALIZE_OUTPUT(Input, o);
            
             o.vect = UnityObjectToClipPos(v.vertex);
             o.uv = TRANSFORM_TEX(v.texcoord1 , _MainTex);
        }
      
        void surf (Input IN, inout SurfaceOutputStandard o)
        {
                float2 screenuv = IN.vect.xy / _ScreenParams.xy;
                float screenDepth = Linear01Depth(tex2D(_CameraDepthTexture, screenuv));
                float diff = screenDepth - Linear01Depth(IN.vect.z);
                float intersect = 1 - smoothstep(0, _ProjectionParams.w * _FadeLength, screenDepth);

            fixed3 Pos = IN.worldPos;
            half gr = getgradient(Pos*_Scale);
            
            o.Albedo = saturate((gr*_Magnitude + (1 - intersect)) + length(IN.screenPos.xy - float2(0.5, 0.5))*2*IN.screenPos.z/_Radius);
               
            clip(o.Albedo.r - _DissolvePercentage);
            
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = 1;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
