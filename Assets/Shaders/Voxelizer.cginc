// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Geometry voxelizer effect
// https://github.com/keijiro/GVoxelizer

#include "Common.cginc"
#include "UnityGBuffer.cginc"
#include "UnityStandardUtils.cginc"
#include "SimplexNoise3D.hlsl"

// Cube map shadow caster; Used to render point light shadows on platforms
// that lacks depth cube map support.
#if defined(SHADOWS_CUBE) && !defined(SHADOWS_CUBE_IN_DEPTH_TEX)
#define PASS_CUBE_SHADOWCASTER
#endif

// Base properties
half4 _Color;
sampler2D _MainTex;
float4 _MainTex_ST;

sampler2D _NormalTex;
float4 _NormalTex_ST;

sampler2D _SpecularTex;
float4 _SpecularTex_ST;

half _Glossiness;
half _Metallic;

// Effect properties
half4 _Color2;
half _ScaleToCentre;
half _Scale;

// Edge properties
half3 _EdgeColor;

// Dynamic properties
float4 _EffectVector;

float _EffectPos;

// Unity Defined Variables;
uniform float4 _LightColor0;

// Vertex input attributes
struct Attributes
{
    float4 position : POSITION;
    float3 normal : NORMAL;
    float2 texcoord : TEXCOORD;
    float4 tangent: TANGENT;
};

// Fragment varyings
struct Varyings
{
    float4 position : SV_POSITION;

#if defined(PASS_CUBE_SHADOWCASTER)
    // Cube map shadow caster pass
    float3 shadow : TEXCOORD0;

#elif defined(UNITY_PASS_SHADOWCASTER)
    // Default shadow caster pass

#else
    // GBuffer construction pass
    float3 normal : NORMAL;
    float2 texcoord : TEXCOORD0;
    half3 ambient : TEXCOORD1;
    float4 edge : TEXCOORD2; // barycentric coord (xyz), emission (w)
    float4 wpos_ch : TEXCOORD3; // world position (xyz), channel select (w)
    
    float3 normalWorld: TEXCOORD5;
	float3 tangentWorld: TEXCOORD6;
	float3 binormalWorld: TEXCOORD7;
#endif
};

//
// Vertex stage
//

Attributes Vertex(Attributes input)
{
    // Only do object space to world space transform.
    input.position = mul(unity_ObjectToWorld, input.position);
    //input.position.xyz *= 1 + saturate(0.5 - abs(_EffectVector.w));
    input.normal = UnityObjectToWorldNormal(input.normal);
    
    return input;
}

//
// Geometry stage
//

Varyings VertexOutput(float3 wpos, half3 wnrm, float2 uv,
                      float4 edge = 0.5, float channel = 0)
{
    Varyings o;

#if defined(PASS_CUBE_SHADOWCASTER)
    // Cube map shadow caster pass: Transfer the shadow vector.
    o.position = UnityWorldToClipPos(float4(wpos, 1));
    o.shadow = wpos - _LightPositionRange.xyz;

#elif defined(UNITY_PASS_SHADOWCASTER)
    // Default shadow caster pass: Apply the shadow bias.
    float scos = dot(wnrm, normalize(UnityWorldSpaceLightDir(wpos)));
    wpos -= wnrm * unity_LightShadowBias.z * sqrt(1 - scos * scos);
    o.position = UnityApplyLinearShadowBias(UnityWorldToClipPos(float4(wpos, 1)));

#else
    // GBuffer construction pass
    o.position = UnityWorldToClipPos(float4(wpos, 1));
    o.normal = wnrm;
    o.texcoord = uv;
    o.ambient = ShadeSHPerVertex(wnrm, 0);
    o.edge = edge;
    o.wpos_ch = float4(wpos, channel);
    
    float3 c1 = cross(wnrm, float3(0.0, 0.0, 1.0));
    float3 c2 = cross(wnrm, float3(0.0, 1.0, 0.0));
    float3 tangent;
    
    if (length(c1)>length(c2))
    {
        tangent = c1;
    }
    else
    {
        tangent = c2;
    }
    tangent = normalize(tangent);
    
    o.normalWorld = normalize( mul( float4(wnrm, 0.0 ), unity_WorldToObject ).xyz );		
	o.tangentWorld = normalize( mul( unity_ObjectToWorld, tangent).xyz );
	o.binormalWorld = normalize( cross( o.normalWorld, o.tangentWorld) );
#endif
    return o;
}

Varyings CubeVertex(
    float3 wpos, half3 wnrm_tri, half3 wnrm_cube, float2 uv,
    float3 bary_tri, float2 bary_cube, float morph, float emission
)
{
    float3 wnrm = normalize(lerp(wnrm_tri, wnrm_cube, morph));
    float3 bary = lerp(bary_tri, float3(bary_cube, 0.5), morph);
    return VertexOutput(wpos, wnrm, uv, float4(bary, emission), 1);
}

/*
Varyings TriangleVertex(float3 wpos, half3 wnrm, float2 uv, float3 bary, float emission)
{
    return VertexOutput(wpos, wnrm, uv, float4(bary, emission), 0);
}*/

[maxvertexcount(24)]
void Geometry(
    triangle Attributes input[3], uint pid : SV_PrimitiveID,
    inout TriangleStream<Varyings> outStream
)
{
    // Input vertices
    float3 p0 = input[0].position.xyz;
    float3 p1 = input[1].position.xyz;
    float3 p2 = input[2].position.xyz;

    float3 n0 = input[0].normal;
    float3 n1 = input[1].normal;
    float3 n2 = input[2].normal;

    float2 uv0 = input[0].texcoord;
    float2 uv1 = input[1].texcoord;
    float2 uv2 = input[2].texcoord;

    float3 center = (p0 + p1 + p2) / 3;

    // Deformation parameter
    float param = 1 - dot(_EffectVector.xyz, center) + _EffectPos;

    // Pass through the vertices if deformation hasn't been started yet.
    if (param < 0)
    {
        outStream.Append(VertexOutput(p0, n0, uv0));
        outStream.Append(VertexOutput(p1, n1, uv1));
        outStream.Append(VertexOutput(p2, n2, uv2));
        outStream.RestartStrip();
        return;
    }

    // Draw nothing at the end of deformation.
    if (param >= 1) return;

    // Choose cube/triangle randomly.
    uint seed = pid * 877;
  
    center += normalize(center)*fixed3(_ScaleToCentre, 0, _ScaleToCentre)*param;
    {
        // -- Cube fx --
        // Base triangle -> Expand -> Morph into cube -> Stretch and fade out

        // Triangle animation (simply expand from the centroid)
        float t_anim =1;// 1 + smoothstep(0.25, 0.5, param) * 60;
        float3 t_p0 = lerp(center, p0, t_anim);
        float3 t_p1 = lerp(center, p1, t_anim);
        float3 t_p2 = lerp(center, p2, t_anim);

        // Cube animation
        float rnd = Random(seed + 1); // random number, gradient noise
        float4 snoise = snoise_grad(float3(rnd * 2378.34, param * 0.8, 0));
       // snoise.xyz = 0;
        float move = saturate(param * 4 - 3); // stretch/move param
        move = move * move;

        float3 pos = center + snoise.xyz*0.05; // cube position
        pos.y += move * snoise.y*0.1;

        float3 scale = float2(1 - move, 1 + move * 5).xyx; // cube scale anim
        scale *= 0.05 * saturate(1 + snoise.w * 2) + _Glossiness*param;
        scale = _Scale* lerp(1.1, 0.4, abs(param - 0.5)*2);
        float edge = saturate(param*2); // Edge color (emission power)

        // Cube points calculation
        float morph = smoothstep(0.1, 0.60, param);
        float3 c_p0 = lerp(t_p2, pos + float3(-1, -1, -1) * scale, morph);
        float3 c_p1 = lerp(t_p2, pos + float3(+1, -1, -1) * scale, morph);
        float3 c_p2 = lerp(t_p0, pos + float3(-1, +1, -1) * scale, morph);
        float3 c_p3 = lerp(t_p1, pos + float3(+1, +1, -1) * scale, morph);
        float3 c_p4 = lerp(t_p2, pos + float3(-1, -1, +1) * scale, morph);
        float3 c_p5 = lerp(t_p2, pos + float3(+1, -1, +1) * scale, morph);
        float3 c_p6 = lerp(t_p0, pos + float3(-1, +1, +1) * scale, morph);
        float3 c_p7 = lerp(t_p1, pos + float3(+1, +1, +1) * scale, morph);

        // Vertex outputs
        float3 c_n = float3(-1, 0, 0);
        outStream.Append(CubeVertex(c_p2, n0, c_n, uv0, float3(0, 0, 1), float2(0, 0), morph, edge));
        outStream.Append(CubeVertex(c_p0, n2, c_n, uv2, float3(1, 0, 0), float2(1, 0), morph, edge));
        outStream.Append(CubeVertex(c_p6, n0, c_n, uv0, float3(0, 0, 1), float2(0, 1), morph, edge));
        outStream.Append(CubeVertex(c_p4, n2, c_n, uv2, float3(1, 0, 0), float2(1, 1), morph, edge));
        outStream.RestartStrip();

        c_n = float3(1, 0, 0);
        outStream.Append(CubeVertex(c_p1, n2, c_n, uv2, float3(0, 0, 1), float2(0, 0), morph, edge));
        outStream.Append(CubeVertex(c_p3, n1, c_n, uv1, float3(1, 0, 0), float2(1, 0), morph, edge));
        outStream.Append(CubeVertex(c_p5, n2, c_n, uv2, float3(0, 0, 1), float2(0, 1), morph, edge));
        outStream.Append(CubeVertex(c_p7, n1, c_n, uv1, float3(1, 0, 0), float2(1, 1), morph, edge));
        outStream.RestartStrip();

        c_n = float3(0, -1, 0);
        outStream.Append(CubeVertex(c_p0, n2, c_n, uv2, float3(1, 0, 0), float2(0, 0), morph, edge));
        outStream.Append(CubeVertex(c_p1, n2, c_n, uv2, float3(1, 0, 0), float2(1, 0), morph, edge));
        outStream.Append(CubeVertex(c_p4, n2, c_n, uv2, float3(1, 0, 0), float2(0, 1), morph, edge));
        outStream.Append(CubeVertex(c_p5, n2, c_n, uv2, float3(1, 0, 0), float2(1, 1), morph, edge));
        outStream.RestartStrip();

        c_n = float3(0, 1, 0);
        outStream.Append(CubeVertex(c_p3, n1, c_n, uv1, float3(0, 0, 1), float2(0, 0), morph, edge));
        outStream.Append(CubeVertex(c_p2, n0, c_n, uv0, float3(1, 0, 0), float2(1, 0), morph, edge));
        outStream.Append(CubeVertex(c_p7, n1, c_n, uv1, float3(0, 0, 1), float2(0, 1), morph, edge));
        outStream.Append(CubeVertex(c_p6, n0, c_n, uv0, float3(1, 0, 0), float2(1, 1), morph, edge));
        outStream.RestartStrip();

        c_n = float3(0, 0, -1);
        outStream.Append(CubeVertex(c_p1, n2, c_n, uv2, float3(0, 0, 1), float2(0, 0), morph, edge));
        outStream.Append(CubeVertex(c_p0, n2, c_n, uv2, float3(0, 0, 1), float2(1, 0), morph, edge));
        outStream.Append(CubeVertex(c_p3, n1, c_n, uv1, float3(0, 1, 0), float2(0, 1), morph, edge));
        outStream.Append(CubeVertex(c_p2, n0, c_n, uv0, float3(1, 0, 0), float2(1, 1), morph, edge));
        outStream.RestartStrip();

        c_n = float3(0, 0, 1);
        outStream.Append(CubeVertex(c_p4, -n2, c_n, uv2, float3(0, 0, 1), float2(0, 0), morph, edge));
        outStream.Append(CubeVertex(c_p5, -n2, c_n, uv2, float3(0, 0, 1), float2(1, 0), morph, edge));
        outStream.Append(CubeVertex(c_p6, -n0, c_n, uv0, float3(0, 1, 0), float2(0, 1), morph, edge));
        outStream.Append(CubeVertex(c_p7, -n1, c_n, uv1, float3(1, 0, 0), float2(1, 1), morph, edge));
        outStream.RestartStrip();
    }
}

//
// Fragment phase
//
#if !defined(UNITY_PASS_SHADOWCASTER)
float4 Fragment2(Varyings input) : SV_Target { 

    float3 bcc = input.edge.xyz;
    float3 fw = fwidth(bcc)*2;
    float3 edge3 = min(smoothstep(fw / 2, fw,     bcc),
                       smoothstep(fw / 2, fw, 1 - bcc));
    float edge = 1 - min(min(edge3.x, edge3.y), edge3.z);

    half3 albedo = tex2D(_MainTex, input.texcoord).rgb * _Color.rgb;

     //dge -= input.edge.w;
        
    //edge = edge*saturate(1 + abs(_EffectPos)) + 0.5*saturate(1 + abs(_EffectPos));
    // Output ambient light and edge emission to the emission buffer.
    
    half3 tnormal = UnpackNormal(tex2D(_NormalTex, input.texcoord));
    
                     // transform normal from tangent to world space
     half3 worldNormal;
     worldNormal.x = dot(input.normalWorld, tnormal);
     worldNormal.y = dot(input.tangentWorld, tnormal);
     worldNormal.z = dot(input.binormalWorld, tnormal);

// Normal Transpose Matrix
    float3x3 local2WorldTranspose = float3x3(
        input.tangentWorld,
        input.binormalWorld,
        input.normalWorld
    );
    
    // Calculate Normal Direction
    float3 normalDirection = normalize( mul( tnormal, local2WorldTranspose ) );
    
    float scos = dot(normalDirection, normalize( _WorldSpaceLightPos0.xyz))*0.5 + 0.5;
    
    half3 sh = ShadeSHPerPixel(input.normal, input.ambient, input.wpos_ch.xyz);

    float3 viewDirection = normalize( _WorldSpaceCameraPos.xyz - input.wpos_ch.xyz);
    float3 lightDirection;
    float atten = 1;
        
    if( _WorldSpaceLightPos0.w == 0.0 ) { // Directional Light
        atten = 1.5;
         lightDirection = normalize( _WorldSpaceLightPos0.xyz );
     } else {
         float3 fragmentToLightSource = _WorldSpaceLightPos0.xyz -  input.wpos_ch.xyz;
         float distance = length( fragmentToLightSource );
         atten = 0.5 / distance;
         lightDirection = normalize( fragmentToLightSource );
     }

    float3 diffuseReflection = atten * _LightColor0.rgb * saturate(dot(normalDirection, lightDirection )*0.5 + 0.5);
    float3 specularReflection = diffuseReflection  * tex2D(_SpecularTex, input.texcoord).rgb * pow( saturate( dot( reflect( -lightDirection, normalDirection ), viewDirection ) ), 10);
     
    float3 lightFinal = diffuseReflection + specularReflection;
     
    half4 outEmission = half4((albedo.rgb  * (1-edge)*scos ) + (edge)* (_EdgeColor.rgb  + _EdgeColor.rgb* input.edge.w), edge);
    outEmission.rgb *= lightFinal;
    return outEmission; 
}
#endif

#if defined(PASS_CUBE_SHADOWCASTER)

// Cube map shadow caster pass
half4 Fragment(Varyings input) : SV_Target
{
    float depth = length(input.shadow) + unity_LightShadowBias.x;
    return UnityEncodeCubeShadowDepth(depth * _LightPositionRange.w);
}

#elif defined(UNITY_PASS_SHADOWCASTER)

// Default shadow caster pass
half4 Fragment() : SV_Target { return 0; }

#elif defined(UNITY_PASS_FORWARDBASE) || defined(UNITY_PASS_FORWARDADD)

// Default shadow caster pass
float4 Fragment(Varyings input) : SV_Target { 

    return float4(1,0,1,1); 
}

#else

// GBuffer construction pass
void Fragment(
    Varyings input,
    out half4 outGBuffer0 : SV_Target0,
    out half4 outGBuffer1 : SV_Target1,
    out half4 outGBuffer2 : SV_Target2,
    out half4 outEmission : SV_Target3
)
{
    half3 albedo = tex2D(_MainTex, input.texcoord).rgb * _Color.rgb;

    // Detect fixed-width edges with using screen space derivatives of
    // barycentric coordinates.
    float3 bcc = input.edge.xyz;
    float3 fw = fwidth(bcc);
    float3 edge3 = min(smoothstep(fw / 2, fw,     bcc),
                       smoothstep(fw / 2, fw, 1 - bcc));
    float edge = 1 - min(min(edge3.x, edge3.y), edge3.z);

    // Update the GBuffer.
    UnityStandardData data;
    float ch = input.wpos_ch.w;
    data.diffuseColor = lerp(albedo,  _Color2.rgb, ch);
    data.occlusion = 1;
    data.specularColor = lerp(albedo, _Color2.rgb, ch);
    data.smoothness = lerp(_Glossiness, _Glossiness, ch);
    data.normalWorld = input.normal;
    UnityStandardDataToGbuffer(data, outGBuffer0, outGBuffer1, outGBuffer2);

    // Output ambient light and edge emission to the emission buffer.
    half3 sh = ShadeSHPerPixel(data.normalWorld, input.ambient, input.wpos_ch.xyz);
    outEmission = half4(sh * data.diffuseColor + _EdgeColor * input.edge.w * edge, 1);
}
#endif
