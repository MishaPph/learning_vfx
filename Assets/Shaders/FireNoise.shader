﻿Shader "Unlit/FireNoise"
{
//https://www.pinterest.com.au/pin/464433780317396307
    Properties
    {
        _Noise ("Noise", 2D) = "white" {}
        _Distortion ("Distortion", 2D) = "white" {}
        _ColorTop("ColorTop", Color) = (0,0,0,0)
        _ColorBottom("ColorBottom", Color) = (0,0,0,0)
        _Edge("Edge", Range(0, 2)) = 0.15
        _Fire("Fire", Range(0, 2)) = 0.15
        _UVOffest("UVOffest", Range(0, 2)) = 0.5
        _Speed("Speed", Range(0, 6)) = 0.5  
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" }
        LOD 100

        Cull Off
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _Noise;
            float4 _Noise_ST;

            sampler2D _Distortion;
            float4 _Distortion_ST;
            
            fixed4 _ColorBottom;
            fixed4 _ColorTop;
            fixed _Speed;
            fixed _Edge, _Fire, _UVOffest;
                 
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _Noise);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 distort = tex2D(_Distortion, i.uv - _Time.xy*0.5);
                fixed4 result =  tex2D(_Noise, fixed2(i.uv.x - distort.y - _CosTime.x, i.uv.y - (distort.x + _Time.y)*_Speed));
                fixed4 gradient = lerp(_ColorBottom, _ColorTop,  i.uv.y);
                
                fixed a = result.x - (i.uv.y* _UVOffest);
                clip(a);
                fixed4 col = result*i.uv.y + gradient;
                fixed rim = saturate((a - (_Edge + i.uv.y))*10) - _Fire;
                return col + rim;
            }
            ENDCG
        }
    }
}
