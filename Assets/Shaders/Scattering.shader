﻿Shader "Unlit/Scattering"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 mult : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                
                 float3 forward = mul((float3x3)unity_CameraToWorld, float3(0,0,1));
                float3 viewDir = UNITY_MATRIX_IT_MV[2].xyz;
                
                o.mult.x = dot(v.vertex.xyz, forward);
                o.mult.y = length(o.vertex.xyz);//  dot(v.vertex, viewDir);
                
                half3 worldNormal = UnityObjectToWorldNormal(v.vertex);
                o.mult.z = dot(worldNormal, _WorldSpaceLightPos0.xyz);
                 o.mult.w =1;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
               
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                col.rgb =  i.mult.rgb; //abs(dot(i.vertex, _WorldSpaceLightPos0.xyz));
                
                return col;
            }
            ENDCG
        }
    }
}
