﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Unlit/BillboardAngle"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	
	SubShader
	{
		Tags{ "Queue" = "AlphaTest" "IgnoreProjector" = "True" "RenderType" = "TransparentCutOut" "DisableBatching" = "True" }

		ZWrite off
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 pos : SV_POSITION;
				float4 color : COLOR0;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv.xy;


                float3 worldNormal = mul( unity_WorldToObject, float4(v.normal, 0.0 ) ).xyz;
                float3 viewNormal = mul( UNITY_MATRIX_V, float4(v.normal, 0.0 ) ).xyz;

				o.pos = mul(UNITY_MATRIX_P,
				mul(UNITY_MATRIX_MV, float4(0.0, 0.0, 0.0, 1.0))
				+ float4(v.vertex.x, v.vertex.y, 0.0, 0.0)
				* float4(1, 1, 1.0, 1.0));
                half3 direct = worldNormal.xyz - viewNormal.xyz;
                
                half lon = atan2(direct.z, direct.x)/UNITY_PI;

				o.uv = (v.uv.xy + half2(floor(lon/_MainTex_ST.x), 0)) * _MainTex_ST.xy;
				
				o.color = float4(abs(lon).xxx, frac(lon/_MainTex_ST.x));
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = tex2D(_MainTex, i.uv);
				
				fixed4 col2 = tex2D(_MainTex, i.uv + _MainTex_ST.xy);
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				fixed4 result = lerp(col, col2,smoothstep(0, 1, i.color.a*8 - i.uv.x));
				clip(result.a - 0.001f);
				return result;
			}
			ENDCG
		}
	}
}