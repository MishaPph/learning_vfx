﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigurableTransparentUnlit.shader" company="Supyrb">
//   Copyright (c) 2019 Supyrb. All rights reserved.
// </copyright>
// <repository>
//   https://github.com/supyrb/ConfigurableShaders
// </repository>
// <author>
//   Johannes Deml
//   send@johannesdeml.com
// </author>
// <documentation>
//   https://github.com/supyrb/ConfigurableShaders/wiki/Stencil
// </documentation>
// --------------------------------------------------------------------------------------------------------------------
Shader "Custom/Mult_Transparent"
{
	Properties
	{
		[HDR] _Color("Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		 _Speed1 ("Speed1", Vector) = (1,1,1,1)
		  
	    _Texture2 ("Texture2 (RGB)", 2D) = "white" {}
        _Speed2 ("Speed2", Vector) = (1,1,1,1)
        
        _Texture3 ("Texture3 (RGB)", 2D) = "white" {}
        _Speed3 ("Speed3", Vector) = (1,1,1,1)
        
        
        _Texture4 ("Texture4 (RGB)", 2D) = "white" {}
        _Speed4 ("Speed4", Vector) = (1,1,1,1)
        
        
		[HeaderHelpURL(Rendering, https, github.com supyrb ConfigurableShaders wiki Rendering)]
		[Tooltip(Changes the depth value. Negative values are closer to the camera)] _Offset("Offset", Float) = 0.0
		[Enum(UnityEngine.Rendering.CullMode)] _Culling ("Cull Mode", Int) = 2
		[Enum(Off,0,On,1)] _ZWrite("ZWrite", Int) = 0
		[Enum(UnityEngine.Rendering.CompareFunction)] _ZTest ("ZTest", Int) = 4
		[Enum(None,0,Alpha,1,Red,8,Green,4,Blue,2,RGB,14,RGBA,15)] _ColorMask("Color Mask", Int) = 14

		[HeaderHelpURL(Blending, https, github.com supyrb ConfigurableShaders wiki Blending)]
		[Enum(UnityEngine.Rendering.BlendMode)] _BlendSrc ("Blend mode Source", Int) = 5
		[Enum(UnityEngine.Rendering.BlendMode)] _BlendDst ("Blend mode Destination", Int) = 10
	}
			
	SubShader
	{
		Tags { "Queue" = "Transparent" "RenderType" = "Transparent"}
		Pass
		{
			LOD 200
			Cull [_Culling]
			Offset [_Offset], [_Offset]
			ZWrite [_ZWrite]
			ZTest [_ZTest]
			ColorMask [_ColorMask]
			Blend [_BlendSrc] [_BlendDst]
			
			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			ENDCG
		}
	}
	CGINCLUDE
	#include "UnityCG.cginc"
	
	half4 _Color;
	sampler2D _MainTex;
	float4 _MainTex_ST;
	
	sampler2D _Texture2;
	float4 _Texture2_ST;
	
	
	sampler2D _Texture3;
	float4 _Texture3_ST;
	
	sampler2D _Texture4;
	float4 _Texture4_ST;
	
	half4 _Speed1, _Speed2, _Speed3, _Speed4;
	
	struct appdata_t {
		float4 vertex : POSITION;
		float2 texcoord : TEXCOORD0;
		float2 texcoord1 : TEXCOORD1;
		float2 texcoord2 : TEXCOORD2;
		float2 texcoord3 : TEXCOORD3;
		half4 color: COLOR;
		UNITY_VERTEX_INPUT_INSTANCE_ID
	};
	
	struct v2f
	{
		float4 vertex : SV_POSITION;
		float2 texcoord : TEXCOORD0;
		float2 texcoord1 : TEXCOORD1;
		float2 texcoord2 : TEXCOORD2;
		float2 texcoord3 : TEXCOORD3;
		half4 color: COLOR;
		#ifdef SOFTPARTICLES_ON
		float4 projPos : TEXCOORD4;
		#endif
	};
	
	v2f vert (appdata_t v)
	{
		v2f o;
		UNITY_SETUP_INSTANCE_ID(v);
		UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
		o.texcoord1 = TRANSFORM_TEX(v.texcoord1, _Texture2);
		o.texcoord2 = TRANSFORM_TEX(v.texcoord2, _Texture3);
		o.texcoord3 = TRANSFORM_TEX(v.texcoord3, _Texture4);
						
		o.color = v.color * _Color;
		#ifdef SOFTPARTICLES_ON
		o.projPos = ComputeScreenPos (o.vertex);
		COMPUTE_EYEDEPTH(o.projPos.z);
		#endif
		return o;
	}
	
	half4 frag (v2f i) : SV_Target
	{
		#ifdef SOFTPARTICLES_ON
		float sceneZ = LinearEyeDepth (SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos)));
		float partZ = i.projPos.z;
		float fade = saturate (_InvFade * (sceneZ-partZ));
		i.color.a *= fade;
		#endif
	
		half4 col = tex2D(_MainTex, i.texcoord + _Speed1.xy*_Time.xx) * i.color;
		half4 tex2 = tex2D(_Texture2, i.texcoord1 + _Speed2.xy*_Time.xx);
		half4 tex3 = tex2D(_Texture3, i.texcoord2 + _Speed3.xy*_Time.xx);
		half4 tex4 = tex2D(_Texture4, i.texcoord3 + _Speed4.xy*_Time.xx);
		
		col.rgb = col.rgb*tex2.rgb*2;
		col.rgb = col.rgb*tex3.rgb*2;
		//col.rgb = col.rgb*tex4.rgb;
		
		col.a = col.a * tex2.a*2;
		col.a = col.a * tex3.a*2;
		col.a = col.a * tex4.a;
		
		col.a = floor(col.a*2);
		
		clip(col.a - 0.001);
		return col;
	}	
	ENDCG

}