﻿using UnityEngine;
using System.Collections;

[ExecuteAlways]
public class CRTEffect : MonoBehaviour
{
	public Material material;
 
	// Postprocess the image
	void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		Graphics.Blit(source, destination, material);
	}
}