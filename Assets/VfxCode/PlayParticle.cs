﻿using UnityEngine;

public class PlayParticle : MonoBehaviour
{
    [SerializeField] private ParticleSystem _ps;

    public bool Play = false;
    public bool Stop = false;
    // Update is called once per frame
    void Update()
    {
        if (Play)
        {
            _ps.Play(true);
            Play = false;
        }

        if (Stop)
        {
            _ps.Stop(true);
            Stop = false;
        }
    }
}
