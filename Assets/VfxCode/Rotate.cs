﻿using UnityEngine;

[ExecuteAlways]
public class Rotate : MonoBehaviour
{
    public Vector3 Speed;

    public Space Space;

    private void Update()
    {
        transform.Rotate(Speed, Space);
    }
}
