﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class LookAtPoint : MonoBehaviour
{
    public Vector3 LookAt;
    public Vector3 Axis;

    private void Update()
    {
        var v = transform.position;
        v.Scale(Axis);
        transform.LookAt(LookAt + v);
    }
}
