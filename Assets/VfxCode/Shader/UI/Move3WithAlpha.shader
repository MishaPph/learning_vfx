﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Vfx/Move3WithAlpha"
{
Properties {
 _MainTex("Sprite Texture", 2D) = "white" { }
_Tex2("Texture2", 2D) = "white" { }
_Tex3("Texture3", 2D) = "white" { }
  
 [HDR]_Color ("Color", Color) = (1,1,1,1)
 [PerRendererData]_StencilComp ("Stencil Comparison", Float) = 8
 [PerRendererData]_Stencil ("Stencil ID", Float) = 0
 [PerRendererData]_StencilOp ("Stencil Operation", Float) = 0
 [PerRendererData]_StencilWriteMask ("Stencil Write Mask", Float) = 255
 [PerRendererData]_StencilReadMask ("Stencil Read Mask", Float) = 255
 [PerRendererData]_ColorMask ("Color Mask", Float) = 15
 
 _Data1 ("MoveData1", Vector) = (1,1,1,1)
 _Data2 ("MoveData2", Vector) = (1,1,1,1)
 _Data3 ("MoveData3", Vector) = (1,1,1,1)
 
[Enum(UnityEngine.Rendering.BlendMode)] _SrcBlend ("BlendSource", Float) = 1
[Enum(UnityEngine.Rendering.BlendMode)] _DstBlend ("BlendDestination", Float) = 0
[Enum(UnityEngine.Rendering.CullMode)] _Cull ("Cull", Float) = 0

}
 SubShader {
    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"  "PreviewType"="Plane" "CanUseSpriteAtlas"="True"}
    LOD 100
    
    Blend [_SrcBlend] [_DstBlend]
	Cull  [_Cull]
	Lighting Off 
	ZWrite Off 
	Fog { Color (0,0,0,0) }
    
    Pass {  
     	Stencil {

             ZFail decrWrap
             Ref [_Stencil]
             Comp [_StencilComp]
             Pass [_StencilOp] 
             ReadMask [_StencilReadMask]
             WriteMask [_StencilWriteMask]

            }
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            #include "UnityUI.cginc"

            struct appdata_t {
                fixed4 vertex : POSITION;
                fixed2 texcoord : TEXCOORD0;
                fixed2 texcoord2 : TEXCOORD1;
                fixed2 texcoord3 : TEXCOORD2;
                fixed4  color : COLOR;
            };

            struct v2f {
                fixed4 vertex : SV_POSITION;
                fixed2 texcoord : TEXCOORD0;
                fixed2 texcoord2 : TEXCOORD1;
                fixed2 texcoord3 : TEXCOORD2;
                fixed4  color : COLOR;
            };

            sampler2D _MainTex;
            sampler2D _Tex2;
            sampler2D _Tex3;
            
            fixed4 _Color;
            fixed4 _MainTex_ST;
            fixed4 _Tex2_ST;
            fixed4 _Tex3_ST;
            fixed4 _ClipRect;
            fixed4 _Data1;
            fixed4 _Data2;
            fixed4 _Data3;
            
            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
                o.texcoord2 = TRANSFORM_TEX(v.texcoord2, _Tex2);
                o.texcoord3 = TRANSFORM_TEX(v.texcoord3, _Tex3);
                o.color = v.color;
                return o;
            }
            
            fixed4 frag (v2f i) : SV_Target
            {
                clip(i.color.a * _Color.a - 0.01f);
               
                fixed4 color = tex2D(_MainTex, i.texcoord*_Data1.zw + float2(_Time.x, _Time.x)*_Data1.xy);
                
                fixed4 color2 = tex2D(_Tex2, i.texcoord2*_Data2.zw + float2(_Time.x, _Time.x)*_Data2.xy);
                
                fixed4 color3 = tex2D(_Tex3, i.texcoord3*_Data3.zw + float2(_Time.x, _Time.x)*_Data3.xy);

                color.rgb = (color.rgb *color2.rgb*2)*color3.rgb*2*i.color.rgb * _Color.rgb;
                color.a = saturate((color.a *color2.a*2)*color3.a*2*i.color.a) * _Color.a;
                return color;
            }
        ENDCG
    }
}

}