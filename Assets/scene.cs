﻿using UnityEngine;

public class scene : MonoBehaviour
{

    [SerializeField] private Animator[] _animators;

    [ContextMenuItem("Run", "Run")] public bool R;

    [SerializeField] private ParticleSystem _particle;

    [SerializeField] private Animation _anim;
    
    [SerializeField] private Animation _cameraAnim;
    
    public float _delay = 0.1f;
    
    public void Run()
    {
        _particle.Play(true);
        foreach (var an in _animators)
        {
            an.SetTrigger("Jump");
        }
        Play(_cameraAnim);
        Invoke(nameof(Exe), _delay);
    }

    private void Exe()
    {
        Play(_anim);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Run();
        } 
        if (Input.GetKeyDown(KeyCode.F))
        {
            Revert(_anim);
            Revert(_cameraAnim);
        }
    }

    private static void Revert(Animation anim)
    {
        var str = anim.clip.name;
        anim[str].speed = -anim[str].speed;
        anim[str].time =  anim[str].length;
        anim.Play(str);
    }
    
    private static void Play(Animation anim)
    {
        var str = anim.clip.name;
        anim[str].speed = Mathf.Abs(anim[str].speed);
        anim[str].time =  0;
        anim.Play();
    }
}
